from sklearn.preprocessing import MultiLabelBinarizer


def create_multihots(columns):
    labels = []
    classes = []
    for column in columns:
        col_list = column.tolist()
        col_list = [set(items.lower().strip()) for items in col_list] if len(col_list[0].split(',')) > 1 else [
            [item.lower().strip()]
            for item in
            col_list]
        mlb = MultiLabelBinarizer()
        multihots = mlb.fit_transform(col_list)
        labels.append(multihots)
        classes.append(mlb.classes_)

    return labels, classes


