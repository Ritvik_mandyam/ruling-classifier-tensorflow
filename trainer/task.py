import tensorflow as tf

from model import model_fn


def main():
    tf.logging.set_verbosity('INFO')
    model_fn()


if __name__ == '__main__':
    main()
