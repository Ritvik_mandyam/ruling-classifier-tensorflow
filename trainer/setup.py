from setuptools import setup, find_packages

REQUIRED_PACKAGES = ['tensorflow>=1.4', 'numpy>=1.15.1', 'scikit-learn>=0.19.2', 'pandas>=0.23.4', 'keras>=2.2.2']

setup(
    name='rulings-classifier-tensorflow',
    version='',
    install_requires = REQUIRED_PACKAGES,
    packages=find_packages(),
    url='',
    license='',
    author='Ritvik Mandyam',
    author_email='ritvik.mandyam@gmail.com',
    include_package_data=True,
    description=''
)
