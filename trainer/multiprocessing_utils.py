from queue import Queue
from threading import Thread


class SimpleQueue:
    def __init__(self, max_queue_size=0, num_threads=10):
        self.queue = Queue(max_queue_size)
        self.num_threads = num_threads
        self.output_list = None

    def map(self, work_function, *args):
        self.output_list = [{} for _ in args[0]]

        def _run_work_function(q):
            while not q.empty():
                index, item = q.get()
                item = work_function.__call__(*args)
                self.output_list[index] = item
                q.task_done()

        for i in range(self.num_threads):
            worker = Thread(target=_run_work_function, args=(self.queue,))
            worker.setDaemon(True)
            worker.start()

        counter = 0
        for item in zip(*args):
            self.queue.put((counter, item))

        self.queue.join()
        return self.output_list
