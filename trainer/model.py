import numpy as np
import tensorflow as tf
from keras import Input, Model
from keras.callbacks import TensorBoard
from keras.layers import Embedding, BatchNormalization, LSTM, TimeDistributed, Dense
from keras.optimizers import Adadelta

from data_utils import load_embeddings, load_data, TENSORBOARD_DIR, MODEL_FILE, MAX_NUM_WORDS, MAX_SEQUENCE_LENGTH
from gcloud_utils import GCSCheckpoint

EMBEDDING_DIM = 300


def model_fn():
    tf.logging.info('Running model_fn()')
    data, labels, word_index = load_data()
    embeddings_index = load_embeddings()
    num_words = min(MAX_NUM_WORDS, len(word_index) + 1)
    embedding_matrix = np.zeros((num_words, EMBEDDING_DIM))

    for word, i in word_index.items():
        if i < MAX_NUM_WORDS:
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                # words not found in embedding index will be all-zeros.
                embedding_matrix[i] = embedding_vector

    tf.logging.info('Created embedding matrix, beginning model definition...')

    # load pre-trained word embeddings into an Embedding layer
    # note that we set trainable = False so as to keep the embeddings fixed
    embedding_layer = Embedding(MAX_NUM_WORDS,
                                EMBEDDING_DIM,
                                input_length=MAX_SEQUENCE_LENGTH,
                                trainable=False)
    embedding_layer.build(None)
    embedding_layer.set_weights([embedding_matrix])

    # train an RNN
    input_sequence = Input(shape=(MAX_SEQUENCE_LENGTH,))
    classifier = embedding_layer(input_sequence)
    classifier = BatchNormalization()(
        classifier)
    classifier = LSTM(128, return_sequences=True)(classifier)
    dense_decoder = TimeDistributed(BatchNormalization())(
        classifier)
    dense_decoder = LSTM(128)(dense_decoder)
    dense_decoder = BatchNormalization()(dense_decoder)
    dense_decoder = Dense(1024)(dense_decoder)
    dense_decoder = Dense(512)(dense_decoder)

    # Propagate through Dense layers with softmax and sigmoid activations to predict outputs
    preds = [Dense(2, activation='softmax', name='case_type')(dense_decoder),
             Dense(4, activation='softmax', name='number_of_judges')(dense_decoder),
             Dense(2, activation='softmax', name='writ_petition')(dense_decoder),
             Dense(2, activation='softmax', name='appeal_case')(dense_decoder),
             Dense(2, activation='softmax', name='order_or_judgement')(dense_decoder),
             Dense(2, activation='softmax', name='govt_involved')(dense_decoder),
             Dense(5, activation='softmax', name='govt_party')(dense_decoder),
             Dense(6, activation='softmax', name='petitioner_type')(dense_decoder),
             Dense(6, activation='softmax', name='fundamental_rights_involved')(dense_decoder),
             Dense(17, activation='sigmoid', name='subject_matter')(dense_decoder),
             Dense(6, activation='softmax', name='decision')(dense_decoder),
             Dense(3, activation='softmax', name='costs_imposed')(dense_decoder)]

    model = Model(inputs=input_sequence, outputs=preds)

    tf.logging.info('Defined model, beginning compilation...')

    ada_optimizer = Adadelta()
    losses = {
        'number_of_judges': 'categorical_crossentropy',
        'appeal_case': 'categorical_crossentropy',
        'decision': 'categorical_crossentropy',
        'govt_party': 'categorical_crossentropy',
        'costs_imposed': 'categorical_crossentropy',
        'case_type': 'categorical_crossentropy',
        'govt_involved': 'categorical_crossentropy',
        'writ_petition': 'categorical_crossentropy',
        'order_or_judgement': 'categorical_crossentropy',
        'petitioner_type': 'categorical_crossentropy',
        'subject_matter': 'binary_crossentropy',
        'fundamental_rights_involved': 'categorical_crossentropy'
    }
    model.compile(loss=losses,
                  optimizer=ada_optimizer,
                  metrics=['accuracy'])

    tf.logging.info('Finished compiling model, beginning training...')

    tb_callback = TensorBoard(log_dir=TENSORBOARD_DIR)
    chkpt_callback = GCSCheckpoint(MODEL_FILE, verbose=1)

    callbacks_list = [tb_callback, chkpt_callback]

    model.fit(data, labels, epochs=30, batch_size=16, callbacks=callbacks_list)

    tf.logging.info('Training successfully completed.')


if __name__ == '__main__':
    tf.logging.set_verbosity('INFO')
    model_fn()
