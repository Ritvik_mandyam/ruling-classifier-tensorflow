import pickle

import numpy as np
import pandas as pd
import tensorflow as tf
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer

from multihot_utils import create_multihots
from multiprocessing_utils import SimpleQueue

MAX_SEQUENCE_LENGTH = 60000
MAX_NUM_WORDS = 52763
MAX_NUM_CLASSES = 25
BATCH_SIZE = 100

UNKNOWN_VALUE = 'Unknown'

GLOVE_FILE = '../data/glove.840B.300d.txt'
DATASET_FILE = '../data/dataset.csv'
MODEL_FILE = '../model.h5'
TENSORBOARD_DIR = '../logs'

NUM_THREADS = 8


def load_data():
    tf.logging.info('Loading data...')
    with tf.gfile.Open(DATASET_FILE) as f:
        data = pd.read_csv(f, header='infer').fillna(UNKNOWN_VALUE)

    targets = data.iloc[:, 4:16]
    data = data.iloc[:, 16]

    targets.columns = range(targets.shape[1])

    labels, classes = create_multihots(np.array(targets.transpose().values))

    tf.logging.info('Loaded CSV file, beginning tokenization...')

    tokenizer = Tokenizer(num_words=MAX_NUM_WORDS)
    tokenizer.fit_on_texts(data)
    with open('tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    tf.logging.info('Tokenizer fitting completed, beginning sequence creation...')
    sequences = tokenizer.texts_to_sequences(data)

    word_index = tokenizer.word_index
    tf.logging.info('Sequences created, beginning padding...')
    data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)
    tf.logging.info('Finished loading data.')
    return data, labels, word_index


def load_embeddings():
    tf.logging.info('Loading embeddings...')
    embeddings_index = {}
    with tf.gfile.Open(GLOVE_FILE) as f:
        q = SimpleQueue(512)

        def _process_embedding(line):
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs

        q.map(_process_embedding, f)
        tf.logging.info('Finished loading embeddings.')
    return embeddings_index


if __name__ == '__main__':
    tf.logging.set_verbosity('INFO')
    dataset = load_data()
